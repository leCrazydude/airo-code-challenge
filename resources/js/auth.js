$(function() {
    $('#logout-button').on('click', function() {
        $.ajax({
            url: "/api/logout",
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('token'))
            },
            success: function (result) {
                window.localStorage.removeItem('token');
                window.localStorage.removeItem('user');
                window.location.href = "/";
            },
            error: function (error) {
                toastr.error(error.responseJSON.message);
                window.location.href = "/";
            }
        });
    });

    if (window.location.pathname != '/') {
        return;
    }
    
    if (window.localStorage.getItem('token') != undefined) {
        $.ajax({
            url: "/api/refresh",
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + window.localStorage.getItem('token'))
            },
            success: function (result) {
                window.localStorage.setItem('token', result.authorisation.token);
                window.localStorage.setItem('user', JSON.stringify(result.user));
                window.location.href = "/dashboard";
            },
            error: function (error) {
                toastr.error(error.responseJSON.message);
            }
        });
    } else {
        $('#login-container').show();
    }

    $('#login-toggle').on('click', function() {
        $('#register-container').hide();
        $('#login-container').show();
    });

    $('#register-toggle').on('click', function() {
        $('#login-container').hide();
        $('#register-container').show();
    });

    $('#login-button').on('click', function() {
        var credentials = {
            email: $('#login-form input[name="email"]').val(),
            password: $('#login-form input[name="password"]').val()
        }

        if (credentials.email == '' || credentials.password == '') {
            return;
        }

        $.ajax({
            url: "/api/login",
            type: "POST",
            data: {
                email: credentials.email,
                password: credentials.password
            },
            success: function (result) {
                window.localStorage.setItem('token', result.authorisation.token);
                window.localStorage.setItem('user', JSON.stringify(result.user));
                $('#login-container').hide();
                window.location.href = "/dashboard";
            },
            error: function (error) {
                toastr.error(error.responseJSON.message);
            }
        });
    });

    $('#login-container input').on('keyup', function (e) {
        if (e.key == 'Enter') {
            $('#login-button').trigger("click");
            return false;
        }
    });

    $('#register-button').on('click', function() {
        var credentials = {
            name: $('#register-form input[name="name"]').val(),
            email: $('#register-form input[name="email"]').val(),
            password: $('#register-form input[name="password"]').val()
        }

        if (credentials.name == '' || credentials.email == '' || credentials.password == '') {
            return;
        }

        $.ajax({
            url: "/api/register",
            type: "POST",
            data: {
                name: credentials.name,
                email: credentials.email,
                password: credentials.password
            },
            success: function (result) {
                window.localStorage.setItem('token', result.authorisation.token);
                window.localStorage.setItem('user', JSON.stringify(result.user));
                $('#register-container').hide();
                window.location.href = "/dashboard";
            },
            error: function (error) {
                toastr.error(error.responseJSON.message);
            }
        });
    });

    $('#register-container input').on('keyup', function (e) {
        if (e.key == 'Enter') {
            $('#register-button').trigger("click");
            return false;
        }
    });
});