import './bootstrap';
import './auth.js';
import './dashboard.js';
import '../sass/app.scss';

import { createApp } from 'vue/dist/vue.esm-bundler';

const app = createApp({})

import NavigationComponent from './components/NavigationComponent.vue';
app.component('navigation-component', NavigationComponent);

import DashboardComponent from './components/DashboardComponent.vue';
app.component('dashboard-component', DashboardComponent);

import QuotationComponent from './components/QuotationComponent.vue';
app.component('quotation-component', QuotationComponent);

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)
app.component('font-awesome-icon', FontAwesomeIcon);

app.mount('#app');