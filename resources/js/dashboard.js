$(function() {
    if (window.location.pathname.indexOf('/dashboard') == -1) {
        return;
    }

    if (window.localStorage.getItem('token') == undefined) {
        window.location.href = "/";
    }
});