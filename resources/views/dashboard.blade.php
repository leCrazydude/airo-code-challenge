@extends('layouts.default')

@section('title', 'Dashboard')
@section('nav-redirect', "/dashboard")

@section('content')
    <dashboard-component></dashboard-component>
@stop

@section('navigation')
    <navigation-component></navigation-component>
@stop