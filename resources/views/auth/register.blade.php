<div id="register-container" class="container box">
    <h3 align="center">Register</h3>

    <form method="post" id="register-form" action="{{ url('/api/register') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Enter Name</label>
            <input type="text" name="name" class="form-control" />
        </div>
        <div class="form-group">
            <label>Enter Email</label>
            <input type="email" name="email" class="form-control" />
        </div>
        <div class="form-group">
            <label>Enter Password</label>
            <input type="password" name="password" class="form-control" />
        </div>
        <div class="form-group">
            <input type="button" id="register-button" name="register" class="btn btn-primary" value="Register" />
        </div>
    </form>
    <p>Already have an account?<span id="login-toggle">Log In</span></p>
</div>