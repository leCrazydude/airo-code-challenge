<div id="login-container" class="container box">
    <h3 align="center">Login</h3>

    <form method="post" id="login-form" action="{{ url('/api/login') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Enter Email</label>
            <input type="email" name="email" class="form-control" />
        </div>
        <div class="form-group">
            <label>Enter Password</label>
            <input type="password" name="password" class="form-control" />
        </div>
        <div class="form-group">
            <input type="button" id="login-button" name="login" class="btn btn-primary" value="Login" />
        </div>
    </form>
    <p>Don't have an account?<span id="register-toggle">Register Now</span></p>
</div>