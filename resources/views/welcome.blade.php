@extends('layouts.default')

@section('title', 'Code Challenge')
@section('nav-redirect', "/")

@section('content')
    @include('auth.login')
    @include('auth.register')
@stop