@extends('layouts.default')

@section('title', 'Quotation')
@section('nav-redirect', "/dashboard")

@section('content')
    <quotation-component></quotation-component>
@stop

@section('navigation')
    <navigation-component></navigation-component>
@stop