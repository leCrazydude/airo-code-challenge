<div class="navbar">
   <div class="navbar-inner">
        <a href="@yield('nav-redirect')">
            <img id="logo" src="{{ URL::to('/') }}/media/logo.svg" alt="AIRO Logo">
            <span id="logo-subtitle">Code Challenge Task</span>
        </a>
   </div>
   @if(Route::current()->uri() != '/')
        <nav class="navbar-auth">
            <ul class="auth-menu">
                <li class="{{ (Route::current()->uri() == 'dashboard') ? 'selected' : '' }}"><a href="/dashboard">Dashboard</a></li>
                <li class="{{ (Route::current()->uri() == 'dashboard/quotation-app') ? 'selected' : '' }}"><a href="/dashboard/quotation-app">Quotation App</a></li>
                @yield('navigation')
            </ul>
        </nav>
    @endif
</div>