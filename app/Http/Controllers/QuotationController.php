<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Quotation;
use Illuminate\Support\Facades\Auth;
use App\Traits\Calculate;

class QuotationController extends Controller
{
    use Calculate;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $quotations = Quotation::where('user_id', Auth::user()->id)->get();
        return response()->json([
            'status' => 'success',
            'quotations' => $quotations,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'age' => 'required|string|max:255',
            'currency_id' => 'required|string|max:255',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        $total = $this->getTotal($request->age, $request->start_date, $request->end_date);
        if (!empty($total['error'])) {
            return response()->json([
                'status' => 'fail',
                'message' => $total['message']
            ], 400);
        }
        
        $quotation = Quotation::create([
            'user_id' => Auth::user()->id,
            'age' => $request->age,
            'currency_id' => $request->currency_id,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'total' => $total,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Quotation created successfully',
            'quotation' => [
                'total' => number_format($quotation['total'], 2),
                'currency_id' => $quotation['currency_id'],
                'quotation_id' => $quotation['id']
            ]
        ]);
    }

    public function show($id)
    {
        $quotation = Quotation::where('id', $id)->where('user_id', Auth::user()->id)->get();

        if (empty($quotation)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Quotation not found'
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'quotation' => $quotation,
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'age' => 'required|string|max:255',
            'currency_id' => 'required|string|max:255',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        $quotation = Quotation::where('id', $id)->where('user_id', Auth::user()->id)->first();

        if (empty($quotation)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Quotation not found'
            ], 400);
        }

        $total = $this->getTotal($request->age, $request->start_date, $request->end_date);
        if (!empty($total['error'])) {
            return response()->json([
                'status' => 'fail',
                'message' => $total['message']
            ], 400);
        }

        $quotation->age = $request->age;
        $quotation->currency_id = $request->currency_id;
        $quotation->start_date = $request->start_date;
        $quotation->end_date = $request->end_date;
        $quotation->total = $total;
        $quotation->save();

        $quotation->total = number_format($quotation->total, 2);

        return response()->json([
            'status' => 'success',
            'message' => 'Quotation updated successfully',
            'quotation' => $quotation,
        ]);
    }

    public function destroy($id)
    {
        $quotation = Quotation::where('id', $id)->where('user_id', Auth::user()->id)->first();

        if (empty($quotation)) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Quotation not found'
            ], 400);
        }

        $quotation->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Quotation deleted successfully',
            'quotation' => $quotation,
        ]);
    }
}
