<?php

namespace App\Traits;

use DateTime;
use App\Models\AgeLoad;

trait Calculate
{
    const FIXED_RATE = 3;

    public function getDateDifference($from, $to)
    {
        $date1 = new DateTime($from);
        $date2 = new DateTime($to);
        
        $days = $date2->diff($date1)->format("%a") + 1;
        return $days;
    }

    public function getTotal($age, $start_date, $end_date)
    {
        $total = floatval(0);
        $ages = array_unique(explode(",", $age));

        foreach ($ages as $age) {
            if (($age < 18) || ($age > 70)) {
                return [
                    'error' => true,
                    'message' => 'Age range must be between 18-70'
                ];
            }

            $ageLoad = AgeLoad::where('min', '<=', intval($age))->where('max', '>=', intval($age))->first();
            $days = $this->getDateDifference($start_date, $end_date);

            $total += ($this::FIXED_RATE * $ageLoad->load * $days);
        }

        return floatval($total);
    }
}