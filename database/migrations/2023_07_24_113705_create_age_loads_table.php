<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('age_loads', function (Blueprint $table) {
            $table->id();
            $table->integer('min');
            $table->integer('max');
            $table->decimal('load', 8, 1);
        });

        $data = [
            ['min' => 18, 'max' => 30, 'load' => 0.6],
            ['min' => 31, 'max' => 40, 'load' => 0.7],
            ['min' => 41, 'max' => 50, 'load' => 0.8],
            ['min' => 51, 'max' => 60, 'load' => 0.9],
            ['min' => 61, 'max' => 70, 'load' => 1],
        ];

        DB::table('age_loads')->insert($data);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('age_loads');
    }
};
